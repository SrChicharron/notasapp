import React, { useState, useEffect } from 'react';
import logoImage from '../Assets/logo.png'
function ModalTarea({ closeModal, onTareaAdded }) {
	const [nombreTarea, setNombreTarea] = useState('');
	const [descripcionTarea, setDescripcionTarea] = useState('');
	const [titulosGuardados, setTitulosGuardados] = useState([]);

	const handleCrearTarea = () => {
		saveData();
		closeModal();
		onTareaAdded();
	};
	const saveData = () => {
		const nuevosTitulos = [
			...titulosGuardados,
			{ titulo: nombreTarea, descripcion: descripcionTarea },
		];
		setTitulosGuardados(nuevosTitulos);
		localStorage.setItem('titulos', JSON.stringify(nuevosTitulos));

		alert('Se ha guardado correctamente ');
	};
	useEffect(() => {
		const titulosGuardados = JSON.parse(localStorage.getItem('titulos')) || [];
		setTitulosGuardados(titulosGuardados);
	}, []);

	const titleStyle = {
		color: '#1F487E',
		fontSize: '50px',
		fontWeight: 'bold',
	   
	  };

	return (
		<div
			style={{
			display: 'flex',
			flexDirection: 'column',
			alignItems: 'center',
			justifyContent: 'center',
			height: '40vh', 
		  	}}
			
		>
			<img
    			src={logoImage} 
    			alt='Logo' 
    			style={{
      			width: 100, 
      			marginBottom: -60, 
    			}}
  			/>
			<h1 style={titleStyle}>Crear tarea</h1>
			<div>
				<label style={{ paddingRight: 60 }} for='tituloTarea'>
					Título :
				</label>
				<input
					type='text'
					id='tituloTarea'
					placeholder='Ingresa el titulo de la tarea'
					value={nombreTarea}
					style={{
						height: 40,
						backgroundColor: '#fff',
						borderRadius: 20,
						marginTop: 5,
						marginBottom: 5,
						paddingStart: 20,
						borderColor: '#000',
						borderWidth: 2,
						width: 400,
					}}
					onChange={(e) => setNombreTarea(e.target.value)}
				/>
			</div>
			<div>
				<label style={{ paddingRight: 20 }} for='descripcionTarea'>
					Descripción:
				</label>
				<input
					type='text'
					id='descripcionTarea'
					placeholder='Ingresa la descripcion de la tarea'
					value={descripcionTarea}
					onChange={(e) => setDescripcionTarea(e.target.value)}
					style={{
						height: 40,
						backgroundColor: '#F8F8F8',
						borderRadius: 20,
						marginTop: 5,
						marginBottom: 5,
						paddingStart: 20,
						borderColor: '#000',
						borderWidth: 2,
						width: 400,
					}}
				/>
			</div>
			<button
				style={{
					width: 200,
					height: 70,
					borderRadius: 20,
					alignItems: 'center',
					justifyContent: 'center',
					backgroundColor: '#247BA0',
					color: '#247BA0',
					borderColor: '#247BA0',
					marginTop: 20,
				}}
				onClick={handleCrearTarea}
			>
				<text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>
					{' '}
					Crear Tarea
				</text>
			</button>
		</div>
	);
}
export default ModalTarea;
